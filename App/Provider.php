<?php

namespace App;

abstract class Provider implements ProviderInterface{

    protected $clientId;
    protected $clientSecret;
    protected $redirect_link = "http://oauth-server/cb";

    public function _construct()
    {
    }

    public function route()
    {
        $route = strtok($_SERVER['REQUEST_URI'], '?');
        switch ($route) {
            case '/':
                $this->home();
                break;
            case '/cb':
                $this->cb();
                break;
            case '/auth-success':
                //authSucess();
                break;
            case '/token':
                //token();
                break;
        }
    }

// Authorization Code Process (1/2)
    public function home()
    {
        $link = $this->request_uri . "?client_id=" . $this->clientId . "&redirect_uri=" . $this->data['redirect_uri'] . "&state=provider-" .;
        echo "<a href=\"{$link}\">Connect with ".$this->data['name']."</a>";
    }

// Authorization Code Process (2/2)
    public function cb()
    {
        ["code" => $code, "state" => $rstate] = $_GET;
       
        // Exchange authCode with access_token
        $surl = $this->getSurl();

        
        $result = json_decode(file_get_contents($surl), true);
        ['access_token' => $access_token] = $result;


        // Get userdata with access_token
        $sapi =  "{$this->uri}";
        $rs = curl_init($sapi);
        curl_setopt($rs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($rs, CURLOPT_HEADER, 0);
        curl_setopt($rs, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer {$access_token}"
        ]);

        $result = curl_exec($rs);
        curl_close($rs);
        var_dump($result);
    }
}