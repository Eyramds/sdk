<?php

namespace App;

class Github extends AbstractProvider
{
    protected $data = [
        "name" => "Github-Provider",
        "redirect_uri" => "http://localhost:8080/connect",
    ];

    protected $clientId;
    protected $clientSecret;
    protected $uri = "https://api.github.com/user";
    protected $accessLink = "https://github.com/login/oauth/authorize";
    protected $uriAuth = "https://github.com/login/oauth/access_token";

    public function __construct(string $client_id, string $client_secret)
    {
        $this->clientId = $client_id;
        $this->clientSecret = $client_secret;
    }

    public function getUserData()
    {
        return $this->callback("/me");
    }
}