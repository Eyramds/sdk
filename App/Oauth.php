<?php

namespace App;

class Oauth extends AbstractProvider
{  
    protected $clientId;
    protected $clientSecret;
    protected $uri = "https://graph.facebook.com/v7.0/";
    protected $request_link = "https://www.facebook.com/v7.0/dialog/oauth";
    protected $uriAuth = "https://graph.facebook.com/v7.0/oauth/access_token";

    public function __construct(string $client_id, string $client_secret)
    {
        $this->clientId = $client_id;
        $this->clientSecret = $client_secret;

        $this->request_link = "http://localhost:7070";
        $this->uriAuth = "http://localhost:7070/auth";
        $this->uri = "https://localhost:7071";
        $this->route();
    }

    public function getSurl()
    {
       return "{$this->uriAuth}?grant_type=authorization_code&client_id={$this->client_id}&client_secret={$this->client_secret}&code={$this->code}&redirect_uri={$this->redirect_link}/cb";
    }

    public function getUserData()
    {
        return $this->callback("/me");
    }
}